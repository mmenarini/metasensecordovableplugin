/*
Copyright 2014 Evothings AB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package edu.ucsd.metasense;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.bluetooth.*;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.content.*;
import android.app.Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Iterator;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.os.Build;
import android.os.ParcelUuid;
import android.util.Base64;

import static android.bluetooth.BluetoothAdapter.*;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class BLE extends CordovaPlugin /*implements LeScanCallback*/ {
	// Used by startScan().
	private CallbackContext mScanCallbackContext;

	// Used by reset().
	private CallbackContext mResetCallbackContext;

	// The Android application Context.
	private Context mContext;

	private boolean mRegisteredReceiver = false;

	// Called when the device's Bluetooth powers on.
	// Used by startScan() and connect() to wait for power-on if Bluetooth was off when the function was called.
	private Runnable mOnPowerOn;

	// Used to send error messages to the JavaScript side if Bluetooth power-on fails.
	private CallbackContext mPowerOnCallbackContext;

	// Map of connected devices.
	//HashMap<Integer, GattHandler> mGatt = null;

	// Monotonically incrementing key to the Gatt map.
	int mNextGattHandle = 1;

	// Called each time cordova.js is loaded.
	@Override
	public void initialize(final CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);
		mContext = webView.getContext();

		if(!mRegisteredReceiver) {
			mContext.registerReceiver(new BluetoothStateReceiver(), new IntentFilter(ACTION_STATE_CHANGED));
			mRegisteredReceiver = true;
		}
	}

	// Handles JavaScript-to-native function calls.
	// Returns true if a supported function was called, false otherwise.
	@Override
	public boolean execute(String action, CordovaArgs args, final CallbackContext callbackContext)
		throws JSONException
	{
		//if("startScan".equals(action)) { startScan(args, callbackContext); return true; }
		//else if("stopScan".equals(action)) { stopScan(args, callbackContext); return true; }
		//else if("connect".equals(action)) { connect(args, callbackContext); return true; }
		//else if("close".equals(action)) { close(args, callbackContext); return true; }
		//else if("rssi".equals(action)) { rssi(args, callbackContext); return true; }
		//else if("services".equals(action)) { services(args, callbackContext); return true; }
		//else if("characteristics".equals(action)) { characteristics(args, callbackContext); return true; }
		//else if("descriptors".equals(action)) { descriptors(args, callbackContext); return true; }
		//else if("readCharacteristic".equals(action)) { readCharacteristic(args, callbackContext); return true; }
		//else if("readDescriptor".equals(action)) { readDescriptor(args, callbackContext); return true; }
		//else if("writeCharacteristic".equals(action)) { writeCharacteristic(args, callbackContext); return true; }
		//else if("writeDescriptor".equals(action)) { writeDescriptor(args, callbackContext); return true; }
		//else if("enableNotification".equals(action)) { enableNotification(args, callbackContext); return true; }
		//else if("disableNotification".equals(action)) { disableNotification(args, callbackContext); return true; }
		//else if("testCharConversion".equals(action)) { testCharConversion(args, callbackContext); return true; }
		
		if("listPairedMetaSenseNodes".equals(action)) { listPairedMetaSenseNodes(args, callbackContext); return true; }
		else if("startReading".equals(action)) { startReading(args, callbackContext); return true; }
		else if("stopReading".equals(action)) { stopReading(args, callbackContext); return true; }
		else if("connectedDevice".equals(action)) { connectedDevice(args, callbackContext); return true; }
		else if("writeMessage".equals(action)) { writeMessage(args, callbackContext); return true; }
				
		//else if("reset".equals(action)) { reset(args, callbackContext); return true; }
		return false;
	}

	/**
	* Called when the WebView does a top-level navigation or refreshes.
	*
	* Plugins should stop any long-running processes and clean up internal state.
	*
	* Does nothing by default.
	*
	* Our version should stop any ongoing scan, and close any existing connections.
	*/
//	@Override
//	public void onReset() {
//		if(mScanCallbackContext != null) {
//			BluetoothAdapter a = getDefaultAdapter();
//			a.stopLeScan(this);
//			mScanCallbackContext = null;
//		}
//		if(mGatt != null) {
//			Iterator<GattHandler> itr = mGatt.values().iterator();
//			while(itr.hasNext()) {
//				GattHandler gh = itr.next();
//				if(gh.mGatt != null)
//					gh.mGatt.close();
//			}
//			mGatt.clear();
//		}
//	}

	// Possibly asynchronous.
	// Ensures Bluetooth is powered on, then calls the Runnable \a onPowerOn.
	// Calls cc.error if power-on fails.
	private void checkPowerState(BluetoothAdapter adapter, CallbackContext cc, Runnable onPowerOn) {
		if(adapter == null) {
			return;
		}
		if(adapter.getState() == STATE_ON) {
			// Bluetooth is ON
			onPowerOn.run();
		} else {
			mOnPowerOn = onPowerOn;
			mPowerOnCallbackContext = cc;
			Intent enableBtIntent = new Intent(ACTION_REQUEST_ENABLE);
			cordova.startActivityForResult(this, enableBtIntent, 0);
		}
	}


    // Possibly asynchronous.
    // Ensures Bluetooth gatt is connected with services discovered.
    // Calls cc.error if power-on fails.
    private void checkServicesDiscovered(final BluetoothAdapter adapter, final String address, final CallbackContext cc, final Runnable onServicesDiscovered) {
        checkPowerState(adapter, cc, new Runnable() {
            @Override
            public void run() {
                try {
                    if(allGatts.containsKey(address) && allGattHandlers.containsKey(address)) {
                        MetaSenseGattHandler gh = allGattHandlers.get(address);
                        gh.callbackContext = cc;
                        onServicesDiscovered.run();
                    } else {
                        // Each device connection has a GattHandler, which handles the events the can happen to the connection.
                        // The implementation of the GattHandler class is found at the end of this file.
                        final MetaSenseGattHandler gh = new MetaSenseGattHandler(cc);
                        gh.addConnectionStateChangeTask(new ConnectionStateChangeTask() {
                            @Override
                            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                                if (status != BluetoothGatt.GATT_SUCCESS) {
                                    //cc.error("Error connecting to device");
                                    return;
                                }
                                if (newState != BluetoothGatt.STATE_CONNECTED)
                                    return;
                                if(!gatt.getServices().isEmpty()) {
                                    allGatts.put(address, gatt);
                                    allGattHandlers.put(address, gh);
                                    onServicesDiscovered.run();
                                } else {
                                    gh.addScanningTask(new ScanningTask() {
                                        @Override
                                        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                                            if (status == BluetoothGatt.GATT_SUCCESS) {
                                                allGatts.put(address, gatt);
                                                allGattHandlers.put(address, gh);
                                                onServicesDiscovered.run();
                                            } else {
                                                cc.error("Error scanning for services");
                                            }
                                        }
                                    });
                                    gatt.discoverServices();
                                }
                            }
                        });
                        adapter.getRemoteDevice(address).connectGatt(mContext, true, gh);
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                    cc.error(e.toString());
                }
            }
        });
    }

	// Called whe the Bluetooth power-on request is completed.
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		Runnable onPowerOn = mOnPowerOn;
		CallbackContext cc = mPowerOnCallbackContext;
		mOnPowerOn = null;
		mPowerOnCallbackContext = null;
		if(resultCode == Activity.RESULT_OK) {
			onPowerOn.run();
		} else {
			if(resultCode == Activity.RESULT_CANCELED) {
				cc.error("Bluetooth power-on canceled");
			} else {
				cc.error("Bluetooth power-on failed, code "+resultCode);
			}
		}
	}

	// These three functions each send a JavaScript callback *without* removing the callback context, as is default.

	private void keepCallback(final CallbackContext callbackContext, JSONObject message) {
		PluginResult r = new PluginResult(PluginResult.Status.OK, message);
		r.setKeepCallback(true);
		callbackContext.sendPluginResult(r);
	}

	private void keepCallback(final CallbackContext callbackContext, String message) {
		PluginResult r = new PluginResult(PluginResult.Status.OK, message);
		r.setKeepCallback(true);
		callbackContext.sendPluginResult(r);
	}

	private void keepCallback(final CallbackContext callbackContext, byte[] message) {
		PluginResult r = new PluginResult(PluginResult.Status.OK, message);
		r.setKeepCallback(true);
		callbackContext.sendPluginResult(r);
	}

	// API implementation. See ble.js for documentation.
//	private void startScan(final CordovaArgs args, final CallbackContext callbackContext) {
//		final BluetoothAdapter adapter = getDefaultAdapter();
//		final LeScanCallback self = this;
//		checkPowerState(adapter, callbackContext, new Runnable() {
//			@Override
//			public void run() {
//				if(!adapter.startLeScan(self)) {
//					callbackContext.error("Android function startLeScan failed");
//					return;
//				}
//				mScanCallbackContext = callbackContext;
//			}
//		});
//	}

	// Called during scan, when a device advertisement is received.
//	public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
//		if(mScanCallbackContext == null) {
//			return;
//		}
//		try {
//			//System.out.println("onLeScan "+device.getAddress()+" "+rssi+" "+device.getName());
//			JSONObject o = new JSONObject();
//			o.put("address", device.getAddress());
//			o.put("rssi", rssi);
//			o.put("name", device.getName());
//			o.put("scanRecord", Base64.encodeToString(scanRecord, Base64.NO_WRAP));
//			keepCallback(mScanCallbackContext, o);
//		} catch(JSONException e) {
//			mScanCallbackContext.error(e.toString());
//		}
//	}

	// API implementation.
//	private void stopScan(final CordovaArgs args, final CallbackContext callbackContext) {
//		BluetoothAdapter adapter = getDefaultAdapter();
//		adapter.stopLeScan(this);
//		mScanCallbackContext = null;
//	}

	// API implementation.
//	private void connect(final CordovaArgs args, final CallbackContext callbackContext) {
//		final BluetoothAdapter adapter = getDefaultAdapter();
//		checkPowerState(adapter, callbackContext, new Runnable() {
//			@Override
//			public void run() {
//				try {
//					// Each device connection has a GattHandler, which handles the events the can happen to the connection.
//					// The implementation of the GattHandler class is found at the end of this file.
//					GattHandler gh = new GattHandler(mNextGattHandle, callbackContext);
//					gh.mGatt = adapter.getRemoteDevice(args.getString(0)).connectGatt(mContext, false, gh);
//					// Note that gh.mGatt and this.mGatt are different object and have different types.
//					if(mGatt == null)
//						mGatt = new HashMap<Integer, GattHandler>();
//					Object res = mGatt.put(mNextGattHandle, gh);
//					assert(res == null);
//					mNextGattHandle++;
//					try {
//						JSONObject o = new JSONObject();
//						o.put("deviceHandle",gh.mHandle);
//						//o.put("state", gh.newState);
//						keepCallback(gh.mConnectContext, o);
//					} catch(JSONException e) {
//						e.printStackTrace();
//						assert(false);
//					}
//				} catch(Exception e) {
//					e.printStackTrace();
//					callbackContext.error(e.toString());
//				}
//			}
//		});
//	}

	// API implementation.
//	private void close(final CordovaArgs args, final CallbackContext callbackContext) {
//		try {
//			GattHandler gh = mGatt.get(args.getInt(0));
//			gh.mGatt.close();
//			mGatt.remove(args.getInt(0));
//		} catch(JSONException e) {
//			e.printStackTrace();
//			callbackContext.error(e.toString());
//		}
//	}

	// API implementation.
//	private void rssi(final CordovaArgs args, final CallbackContext callbackContext) {
//		GattHandler gh = null;
//		try {
//			gh = mGatt.get(args.getInt(0));
//			if(gh.mRssiContext != null) {
//				callbackContext.error("Previous call to rssi() not yet completed!");
//				return;
//			}
//			gh.mRssiContext = callbackContext;
//			if(!gh.mGatt.readRemoteRssi()) {
//				gh.mRssiContext = null;
//				callbackContext.error("readRemoteRssi");
//			}
//		} catch(Exception e) {
//			e.printStackTrace();
//			if(gh != null) {
//				gh.mRssiContext = null;
//			}
//			callbackContext.error(e.toString());
//		}
//	}

	// API implementation.
//	private void services(final CordovaArgs args, final CallbackContext callbackContext) {
//		try {
//			final GattHandler gh = mGatt.get(args.getInt(0));
//			gh.mOperations.add(new Runnable() {
//				@Override
//				public void run() {
//					gh.mCurrentOpContext = callbackContext;
//					if(!gh.mGatt.discoverServices()) {
//						gh.mCurrentOpContext = null;
//						callbackContext.error("discoverServices");
//						gh.process();
//					}
//				}
//			});
//			gh.process();
//		} catch(Exception e) {
//			e.printStackTrace();
//			callbackContext.error(e.toString());
//		}
//	}

	// API implementation.
//	private void characteristics(final CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
//		final GattHandler gh = mGatt.get(args.getInt(0));
//		JSONArray a = new JSONArray();
//		for(BluetoothGattCharacteristic c : gh.mServices.get(args.getInt(1)).getCharacteristics()) {
//			if(gh.mCharacteristics == null)
//				gh.mCharacteristics = new HashMap<Integer, BluetoothGattCharacteristic>();
//			Object res = gh.mCharacteristics.put(gh.mNextHandle, c);
//			assert(res == null);
//
//			JSONObject o = new JSONObject();
//			o.put("handle", gh.mNextHandle);
//			o.put("uuid", c.getUuid().toString());
//			o.put("permissions", c.getPermissions());
//			o.put("properties", c.getProperties());
//			o.put("writeType", c.getWriteType());
//
//			gh.mNextHandle++;
//			a.put(o);
//		}
//		callbackContext.success(a);
//	}

	// API implementation.
//	private void descriptors(final CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
//		final GattHandler gh = mGatt.get(args.getInt(0));
//		JSONArray a = new JSONArray();
//		for(BluetoothGattDescriptor d : gh.mCharacteristics.get(args.getInt(1)).getDescriptors()) {
//			if(gh.mDescriptors == null)
//				gh.mDescriptors = new HashMap<Integer, BluetoothGattDescriptor>();
//			Object res = gh.mDescriptors.put(gh.mNextHandle, d);
//			assert(res == null);
//
//			JSONObject o = new JSONObject();
//			o.put("handle", gh.mNextHandle);
//			o.put("uuid", d.getUuid().toString());
//			o.put("permissions", d.getPermissions());
//
//			gh.mNextHandle++;
//			a.put(o);
//		}
//		callbackContext.success(a);
//	}

	// API implementation.
//	private void readCharacteristic(final CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
//		final GattHandler gh = mGatt.get(args.getInt(0));
//		gh.mOperations.add(new Runnable() {
//			@Override
//			public void run() {
//				try {
//					gh.mCurrentOpContext = callbackContext;
//					if(!gh.mGatt.readCharacteristic(gh.mCharacteristics.get(args.getInt(1)))) {
//						gh.mCurrentOpContext = null;
//						callbackContext.error("readCharacteristic");
//						gh.process();
//					}
//				} catch(JSONException e) {
//					e.printStackTrace();
//					callbackContext.error(e.toString());
//					gh.process();
//				}
//			}
//		});
//		gh.process();
//	}

	// API implementation.
//	private void readDescriptor(final CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
//		final GattHandler gh = mGatt.get(args.getInt(0));
//		gh.mOperations.add(new Runnable() {
//			@Override
//			public void run() {
//				try {
//					gh.mCurrentOpContext = callbackContext;
//					if(!gh.mGatt.readDescriptor(gh.mDescriptors.get(args.getInt(1)))) {
//						gh.mCurrentOpContext = null;
//						callbackContext.error("readDescriptor");
//						gh.process();
//					}
//				} catch(JSONException e) {
//					e.printStackTrace();
//					callbackContext.error(e.toString());
//					gh.process();
//				}
//			}
//		});
//		gh.process();
//	}

	// API implementation.
//	private void writeCharacteristic(final CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
//		final GattHandler gh = mGatt.get(args.getInt(0));
//		gh.mOperations.add(new Runnable() {
//			@Override
//			public void run() {
//				try {
//					gh.mCurrentOpContext = callbackContext;
//					BluetoothGattCharacteristic c = gh.mCharacteristics.get(args.getInt(1));
//					System.out.println("writeCharacteristic("+args.getInt(0)+", "+args.getInt(1)+", "+args.getString(2)+")");
//					c.setValue(args.getArrayBuffer(2));
//					if(!gh.mGatt.writeCharacteristic(c)) {
//						gh.mCurrentOpContext = null;
//						callbackContext.error("writeCharacteristic");
//						gh.process();
//					}
//				} catch(JSONException e) {
//					e.printStackTrace();
//					callbackContext.error(e.toString());
//					gh.process();
//				}
//			}
//		});
//		gh.process();
//	}

	// API implementation.
//	private void writeDescriptor(final CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
//		final GattHandler gh = mGatt.get(args.getInt(0));
//		gh.mOperations.add(new Runnable() {
//			@Override
//			public void run() {
//				try {
//					gh.mCurrentOpContext = callbackContext;
//					BluetoothGattDescriptor d = gh.mDescriptors.get(args.getInt(1));
//					d.setValue(args.getArrayBuffer(2));
//					if(!gh.mGatt.writeDescriptor(d)) {
//						gh.mCurrentOpContext = null;
//						callbackContext.error("writeDescriptor");
//						gh.process();
//					}
//				} catch(JSONException e) {
//					e.printStackTrace();
//					callbackContext.error(e.toString());
//					gh.process();
//				}
//			}
//		});
//		gh.process();
//	}

	public static final String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb"; 
	// API implementation.
//	private void enableNotification(final CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
//		final GattHandler gh = mGatt.get(args.getInt(0));
//		BluetoothGattCharacteristic c = gh.mCharacteristics.get(args.getInt(1));
//		gh.mNotifications.put(c, callbackContext);
//		if(!gh.mGatt.setCharacteristicNotification(c, true)) {
//			callbackContext.error("setCharacteristicNotification");
//			return;
//		}
//		BluetoothGattDescriptor descriptor = c.getDescriptor(
//				UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG));
//		descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
//		boolean ret = gh.mGatt.writeDescriptor(descriptor);
//	}

	// API implementation.
//	private void disableNotification(final CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
//		final GattHandler gh = mGatt.get(args.getInt(0));
//		BluetoothGattCharacteristic c = gh.mCharacteristics.get(args.getInt(1));
//		gh.mNotifications.remove(c);
//		if(gh.mGatt.setCharacteristicNotification(c, false)) {
//			callbackContext.success();
//		} else {
//			callbackContext.error("setCharacteristicNotification");
//					return;
//		}
//		BluetoothGattDescriptor descriptor = c.getDescriptor(
//				UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG));
//		descriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
//		boolean ret = gh.mGatt.writeDescriptor(descriptor);
//	}

	// API implementation.
//	private void testCharConversion(final CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
//		byte[] b = {(byte)args.getInt(0)};
//		callbackContext.success(b);
//	}

	// API implementation.
//	private void reset(final CordovaArgs args, final CallbackContext cc) throws JSONException {
//		mResetCallbackContext = null;
//		BluetoothAdapter a = getDefaultAdapter();
//		if(mScanCallbackContext != null) {
//			a.stopLeScan(this);
//			mScanCallbackContext = null;
//		}
//		int state = a.getState();
//		//STATE_OFF, STATE_TURNING_ON, STATE_ON, STATE_TURNING_OFF.
//		if(state == STATE_TURNING_ON) {
//			// reset in progress; wait for STATE_ON.
//			mResetCallbackContext = cc;
//			return;
//		}
//		if(state == STATE_TURNING_OFF) {
//			// reset in progress; wait for STATE_OFF.
//			mResetCallbackContext = cc;
//			return;
//		}
//		if(state == STATE_OFF) {
//			boolean res = a.enable();
//			if(res) {
//				mResetCallbackContext = cc;
//			} else {
//				cc.error("enable");
//			}
//			return;
//		}
//		if(state == STATE_ON) {
//			boolean res = a.disable();
//			if(res) {
//				mResetCallbackContext = cc;
//			} else {
//				cc.error("disable");
//			}
//			return;
//		}
//		cc.error("Unknown state: "+state);
//	}

	// Receives notification about Bluetooth power on and off. Used by reset().
	class BluetoothStateReceiver extends BroadcastReceiver {
		public void onReceive(Context context, Intent intent) {
			BluetoothAdapter a = getDefaultAdapter();
			int state = a.getState();
			System.out.println("BluetoothState: "+a);
			if(mResetCallbackContext != null) {
				if(state == STATE_OFF) {
					boolean res = a.enable();
					if(!res) {
						mResetCallbackContext.error("enable");
						mResetCallbackContext = null;
					}
				}
				if(state == STATE_ON) {
					mResetCallbackContext.success();
					mResetCallbackContext = null;
				}
			}
		}
	};

	/* Running more than one operation of certain types on remote Gatt devices
	* seem to cause it to stop responding.
	* The known types are 'read' and 'write'.
	* I've added 'services' to be on the safe side.
	* 'rssi' and 'notification' should be safe.
	*/

	// This class handles callbacks pertaining to device connections.
	// Also maintains the per-device operation queue.
//	private class GattHandler extends BluetoothGattCallback {
//		// Local copy of the key to BLE.mGatt. Fed by BLE.mNextGattHandle.
//		final int mHandle;
//
//		// The queue of operations.
//		LinkedList<Runnable> mOperations = new LinkedList<Runnable>();
//
//		// connect() and rssi() are handled separately from other operations.
//		CallbackContext mConnectContext, mRssiContext, mCurrentOpContext;
//
//		// The Android API connection.
//		BluetoothGatt mGatt;
//
//		// Maps of integer to Gatt subobject.
//		HashMap<Integer, BluetoothGattService> mServices;
//		HashMap<Integer, BluetoothGattCharacteristic> mCharacteristics;
//		HashMap<Integer, BluetoothGattDescriptor> mDescriptors;
//
//		// Monotonically incrementing key to the subobject maps.
//		int mNextHandle = 1;
//
//		// Notification callbacks. The BluetoothGattCharacteristic object, as found in the mCharacteristics map, is the key.
//		HashMap<BluetoothGattCharacteristic, CallbackContext> mNotifications =
//			new HashMap<BluetoothGattCharacteristic, CallbackContext>();
//
//		GattHandler(int h, CallbackContext cc) {
//			mHandle = h;
//			mConnectContext = cc;
//		}
//
//		// Run the next operation, if any.
//		void process() {
//			if(mCurrentOpContext != null)
//				return;
//			Runnable r = mOperations.poll();
//			if(r == null)
//				return;
//			r.run();
//		}
//
//		@Override
//		public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
//			if(status == BluetoothGatt.GATT_SUCCESS) {
//				try {
//					JSONObject o = new JSONObject();
//					o.put("deviceHandle", mHandle);
//					o.put("state", newState);
//					keepCallback(mConnectContext, o);
//				} catch(JSONException e) {
//					e.printStackTrace();
//					assert(false);
//				}
//			} else {
//				//mConnectContext.error(status);
//
//			}
//		}
//		@Override
//		public void onReadRemoteRssi(BluetoothGatt g, int rssi, int status) {
//			CallbackContext c = mRssiContext;
//			mRssiContext = null;
//			if(status == BluetoothGatt.GATT_SUCCESS) {
//				c.success(rssi);
//			} else {
//				c.error(status);
//			}
//		}
//		@Override
//		public void onServicesDiscovered(BluetoothGatt g, int status) {
//			if(status == BluetoothGatt.GATT_SUCCESS) {
//				List<BluetoothGattService> services = g.getServices();
//				JSONArray a = new JSONArray();
//				for(BluetoothGattService s : services) {
//					// give the service a handle.
//					if(mServices == null)
//						mServices = new HashMap<Integer, BluetoothGattService>();
//					Object res = mServices.put(mNextHandle, s);
//					assert(res == null);
//
//					try {
//						JSONObject o = new JSONObject();
//						o.put("handle", mNextHandle);
//						o.put("uuid", s.getUuid().toString());
//						o.put("type", s.getType());
//
//						mNextHandle++;
//						a.put(o);
//					} catch(JSONException e) {
//						e.printStackTrace();
//						assert(false);
//					}
//				}
//				mCurrentOpContext.success(a);
//			} else {
//				mCurrentOpContext.error(status);
//			}
//			mCurrentOpContext = null;
//			process();
//		}
//		@Override
//		public void onCharacteristicRead(BluetoothGatt g, BluetoothGattCharacteristic c, int status) {
//			if(status == BluetoothGatt.GATT_SUCCESS) {
//				mCurrentOpContext.success(c.getValue());
//			} else {
//				mCurrentOpContext.error(status);
//			}
//			mCurrentOpContext = null;
//			process();
//		}
//		@Override
//		public void onDescriptorRead(BluetoothGatt g, BluetoothGattDescriptor d, int status) {
//			if(status == BluetoothGatt.GATT_SUCCESS) {
//				mCurrentOpContext.success(d.getValue());
//			} else {
//				mCurrentOpContext.error(status);
//			}
//			mCurrentOpContext = null;
//			process();
//		}
//		@Override
//		public void onCharacteristicWrite(BluetoothGatt g, BluetoothGattCharacteristic c, int status) {
//			if(status == BluetoothGatt.GATT_SUCCESS) {
//				mCurrentOpContext.success();
//			} else {
//				mCurrentOpContext.error(status);
//			}
//			mCurrentOpContext = null;
//			process();
//		}
//		@Override
//		public void onDescriptorWrite(BluetoothGatt g, BluetoothGattDescriptor d, int status) {
//			if(status == BluetoothGatt.GATT_SUCCESS) {
//				mCurrentOpContext.success();
//			} else {
//				mCurrentOpContext.error(status);
//			}
//			mCurrentOpContext = null;
//			process();
//		}
//		@Override
//		public void onCharacteristicChanged(BluetoothGatt g, BluetoothGattCharacteristic c) {
//			CallbackContext cc = mNotifications.get(c);
//			keepCallback(cc, c.getValue());
//		}
//	};

    private interface WritingTask {
        void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status);
    }
    private interface ScanningTask {
        void onServicesDiscovered(BluetoothGatt gatt, int status);
    }
	private interface ConnectionStateChangeTask {
		void onConnectionStateChange(BluetoothGatt gatt, int status, int newState);
	}
    //Handles all gatt events uses task to change how events are handled in different moments
    private class MetaSenseGattHandler extends BluetoothGattCallback {
        public CallbackContext callbackContext;

        public boolean reportSuccess = false;
        private LinkedList<WritingTask> writingTasks = new LinkedList<WritingTask>();
        private ScanningTask scanningTask;
		private ConnectionStateChangeTask connectionStateChangeTask;

        private Map<String,BluetoothGattService> allGattServices = new HashMap<String, BluetoothGattService>();


        public MetaSenseGattHandler(CallbackContext callbackContext) {
            this.callbackContext = callbackContext;
        }
        public void addWritingTask(WritingTask t) {
            writingTasks.add(t);
        }
		public void addScanningTask(ScanningTask t) {
			scanningTask =t;
		}
		public void addConnectionStateChangeTask(ConnectionStateChangeTask t) {
			connectionStateChangeTask =t;
		}
		@Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if(!writingTasks.isEmpty()) {
                WritingTask t = writingTasks.removeFirst();
                t.onCharacteristicWrite(gatt,characteristic,status);
            }
        }

		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
			if(connectionStateChangeTask!=null)
				connectionStateChangeTask.onConnectionStateChange(gatt, status, newState);
		}

		@Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            if(status != BluetoothGatt.GATT_SUCCESS)
                callbackContext.error("Could not write descriptor for device.");
            else if(reportSuccess)
                callbackContext.success();
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            keepCallback(callbackContext, characteristic.getValue());
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if(scanningTask!=null)
                scanningTask.onServicesDiscovered(gatt, status);
        }
    }

    private ScheduledExecutorService scheduleTaskExecutor = Executors.newScheduledThreadPool(3);
    private MetaSenseGattHandler currentMetaSenseGattHandler;
    private UUID commService = UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb");
    private UUID characteristicsId = UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb");
    private BluetoothGatt mSelectedGatt;
    private BluetoothGattCharacteristic currentCharacteristics;
    private Map<String,BluetoothGatt> allGatts = new HashMap<String, BluetoothGatt>();
    private Map<String,MetaSenseGattHandler> allGattHandlers = new HashMap<String, MetaSenseGattHandler>();

    private boolean enableNotificationForReads(BluetoothGatt gatt, BluetoothGattCharacteristic c) {
        gatt.setCharacteristicNotification(c,true);
        BluetoothGattDescriptor descriptor = c.getDescriptor(
                UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG));
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        return gatt.writeDescriptor(descriptor);
    }
    private boolean disableNotificationForReads(BluetoothGatt gatt, BluetoothGattCharacteristic c) {
        gatt.setCharacteristicNotification(c,false);
        BluetoothGattDescriptor descriptor = c.getDescriptor(
                UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG));
        descriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
        return gatt.writeDescriptor(descriptor);
    }
    private void performStartReadingWithServices(BluetoothGatt gatt, final CallbackContext callbackContext) {
        BluetoothGattService service =  gatt.getService(commService);
		mSelectedGatt = gatt;
        currentMetaSenseGattHandler = allGattHandlers.get(gatt.getDevice().getAddress());
        if(service!=null) {
            BluetoothGattCharacteristic c = service.getCharacteristic(characteristicsId);
            currentCharacteristics = c;
            if(c!=null) {
                if((c.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0) {
//                    if(((c.getPermissions() & BluetoothGattCharacteristic.PERMISSION_READ) != 0) &&
//                            ((c.getPermissions() & BluetoothGattCharacteristic.PERMISSION_WRITE) != 0)) {
					enableNotificationForReads(gatt,c);
					//if(!enableNotificationForReads(gatt,c)) callbackContext.error("Could not enable notification");
//                    } else callbackContext.error("Do not have the permission to wrad and write this device.");
                } else callbackContext.error("Device does not have the ability to notify new messages. It is not a MetaSense node.");
            } else callbackContext.error("Cannot find the right characteristics in the given device. It is not a MetaSense node.");
        } else callbackContext.error("Cannot find the right service in the given device. It is not a MetaSense node.");
    }
	// API implementation.

/*
    private void doPerformStartReading(BluetoothGatt gatt, MetaSenseGattHandler gh, CallbackContext callbackContext) {
        try {
            // Each device connection has a GattHandler, which handles the events the can happen to the connection.
            // The implementation of the GattHandler class is found at the end of this file.
            currentMetaSenseGattHandler = gh;
            mSelectedGatt = gatt;
            if (mSelectedGatt.getServices()==null || mSelectedGatt.getServices().isEmpty())
                mSelectedGatt.discoverServices();
            else
                performStartReadingWithServices(mSelectedGatt, callbackContext);
        } catch(Exception e) {
            e.printStackTrace();
            callbackContext.error(e.toString());
        }
    }

    private class TaskSemaphore {
        private boolean doTask;
        private Runnable runnable;
        public TaskSemaphore(Runnable runnable) {
            doTask = true;
            this.runnable = runnable;
        }
        public synchronized void doTaskOnce() {
            if(doTask)
                runnable.run();
            doTask = false;
        }
    }

*/

    private void startReading(final CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
        if (mSelectedGatt!=null) {
            try {
                performStopReading();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        final String deviceAddr = toStringAddr(Long.parseLong(args.getString(0)));
        final BluetoothAdapter adapter = getDefaultAdapter();
        checkServicesDiscovered(adapter, deviceAddr, callbackContext, new Runnable() {
            @Override
            public void run() {
                try {
                    performStartReadingWithServices(allGatts.get(deviceAddr) , callbackContext);
                } catch(Exception e) {
                    e.printStackTrace();
                    callbackContext.error(e.toString());
                }
            }
        });

    }
    private void checkDeviceService(String address, final CallbackContext callbackContext) {
        final BluetoothAdapter adapter = getDefaultAdapter();
        BluetoothDevice btd = adapter.getRemoteDevice(address);
        final MetaSenseGattHandler gh = new MetaSenseGattHandler(callbackContext);
        gh.addConnectionStateChangeTask(new ConnectionStateChangeTask() {
            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                final ScanningTask st = new ScanningTask() {
                    @Override
                    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        BluetoothGattService service = gatt.getService(commService);
                        if (service != null) {
                            allGatts.put(gatt.getDevice().getAddress(),gatt);
                            allGattHandlers.put(gatt.getDevice().getAddress(),gh);
                        }
                    }
                    }
                };
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    gh.addScanningTask(st);
                    if (!gatt.getServices().isEmpty())
                        st.onServicesDiscovered(gatt, BluetoothGatt.GATT_SUCCESS);
                    else
                        gatt.discoverServices();
                }
            }
        });
        BluetoothGatt gatt= btd.connectGatt(mContext, true, gh);
    }
	
	private String toStringAddr(long addr) {
		String hexAddr = Long.toHexString(addr);
        Pattern re = Pattern.compile(".{2}");
        Matcher m = re.matcher("000000000000".substring(0, 12 - hexAddr.length()) + hexAddr);
        StringBuilder sb = new StringBuilder();
        int mIdx = 0;
        while (m.find()){
            for( int groupIdx = 0; groupIdx < m.groupCount()+1; groupIdx++ ){
                if (mIdx > 0)
                    sb.append(":");
                sb.append(m.group(groupIdx).toUpperCase());
            }
            mIdx++;
        }
    	return sb.toString();
	}

	private long toLongAddr(String addr) {
		StringBuilder b = new StringBuilder();
        for (String s : addr.split(":")) {
			b.append(s);
        }
		return Long.valueOf(b.toString(),16).longValue();
	}

	
	// API implementation.
	private void listPairedMetaSenseNodes(final CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
		final BluetoothAdapter adapter = getDefaultAdapter();
        checkPowerState(adapter, callbackContext, new Runnable() {
			@Override
			public void run() {
                Set<BluetoothDevice> devs = adapter.getBondedDevices();
                final JSONArray arr = new JSONArray();
                final HashSet<String> checking = new HashSet<String>();
                final HashSet<BluetoothDevice> devices = new HashSet<BluetoothDevice>();
                for (BluetoothDevice btd : devs) {
                    if (!"MetaSense".equals(btd.getName()))
                        continue;
                    devices.add(btd);
                    checking.add(btd.getAddress());
                }
                for (String addr : new ArrayList<String>(checking))
                    if (allGatts.containsKey(addr))
                        checking.remove(addr);

                for (String addr : new ArrayList<String>(checking))
                    checkDeviceService(addr, callbackContext);

                scheduleTaskExecutor.schedule(new Runnable() {
                    @Override
                    public void run() {
                        for (BluetoothGatt gatt : allGatts.values()) {
                            try {
								//String hexAddr = gatt.getDevice().getAddress();
                                //StringBuilder b = new StringBuilder();
                                //for (String s : hexAddr.split(":")) {
                                //    b.append(s);
                                //}
								//long addrLong = Long.valueOf(b.toString(),16).longValue();
								long addrLong = toLongAddr(gatt.getDevice().getAddress());
								
								//System.out.println("onLeScan "+device.getAddress()+" "+rssi+" "+device.getName());
                                JSONObject o = new JSONObject();
                                o.put("address", addrLong);
                                o.put("rssi", 0);
                                o.put("name", gatt.getDevice().getName());
                                o.put("canPair", false);
                                o.put("isReachable", false);
                                o.put("isPaired", true);
                                o.put("deviceid", gatt.getDevice().getAddress());
                                arr.put(o);
                            } catch (JSONException e) {
                                callbackContext.error(e.toString());
                                return;
                            }
                        }
                        callbackContext.success(arr);
                    }
                }, 3, TimeUnit.SECONDS);
            }
		});
	}

    private void performStopReading() {
        if (currentCharacteristics!=null)
            disableNotificationForReads(mSelectedGatt,currentCharacteristics);
        mSelectedGatt.close();
    }
	// API implementation.
	private void stopReading(final CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
		if (mSelectedGatt!=null && currentMetaSenseGattHandler !=null) {
            currentMetaSenseGattHandler.reportSuccess = true;
            currentMetaSenseGattHandler.callbackContext = callbackContext;
            performStopReading();
        }
        mSelectedGatt = null;
        currentCharacteristics = null;
        currentMetaSenseGattHandler = null;
	}

	// API implementation.
	private void connectedDevice(final CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
		if (mSelectedGatt != null)
            callbackContext.success(mSelectedGatt.getDevice().getAddress());
        else
            callbackContext.error("No device selected.");
	}

    private boolean writeMessageFragment(LinkedList<String> stringSet) {
        currentCharacteristics.setValue(stringSet.removeFirst());
        currentCharacteristics.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        if(!mSelectedGatt.writeCharacteristic(currentCharacteristics)) {
            return false;
        }
        return true;
    }
	// API implementation.
	private void writeMessage(final CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
        if (mSelectedGatt != null && currentMetaSenseGattHandler != null) {

            final LinkedList<String> stringSet = new LinkedList<String>();

            String serviceId = args.getString(0);
			final String deviceAddr = toStringAddr(Long.parseLong(args.getString(0)));

            //String deviceInstanceId = args.getString(1);
            String message = args.getString(1);

            Matcher m = Pattern.compile(".{1,20}")
                    .matcher(message);
            while (m.find()) {
                stringSet.add(m.group());
            }

            for (String s : stringSet)
                currentMetaSenseGattHandler.addWritingTask(new WritingTask() {
                    @Override
                    public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                        if (status == BluetoothGatt.GATT_SUCCESS) {
                            if (stringSet.isEmpty())
                                callbackContext.success();
                            else if (!writeMessageFragment(stringSet))
                                callbackContext.error("Could not write to de device.");
                        } else
                                callbackContext.error("Could not discover services for device.");
                    }
                });
            if (!writeMessageFragment(stringSet))
                callbackContext.error("Could not write to de device.");
        }
    }
}
