﻿// -----------------------------------------------------------------------------
// Copyright 2015 Next Wave Sottware, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//	 http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// This is the Windows 8.1 implementation of the com.evothings.ble plugin API.
// All callable endpoints are in this BLE namespace.
// -----------------------------------------------------------------------------
cordova.commandProxy.add("BLE", {

    // exec(win, fail, 'BLE', 'listPairedMetaSenseNodes', []);
    listPairedMetaSenseNodes: function (successCallback, errorCallback) {
        metasense.logger.logApiEntry("listPairedMetaSenseNodes");
        metasense.listPairedMetaSenseNodes(successCallback, errorCallback);
        metasense.logger.logApiExit("listPairedMetaSenseNodes");
    },

    // exec(win, fail, 'BLE', 'startReading', []);
    startReading: function (successCallback, errorCallback, args) {
        metasense.logger.logApiEntry("startReading");
        var address = args[0] ? args[0] : "";
        //var deviceInstanceId = args[1] ? args[1] : "";
        metasense.startReading(address, successCallback, errorCallback);
        metasense.logger.logApiExit("startReading");
    },
	    
	// exec(win, fail, 'BLE', 'startReading', []);
    stopReading: function (successCallback, errorCallback, args) {
        metasense.logger.logApiEntry("stopReading");
        // var serviceId = args[0] ? args[0] : "";
        // var deviceInstanceId = args[1] ? args[1] : "";
        metasense.stopReading(successCallback, errorCallback);
        metasense.logger.logApiExit("stopReading");
    },

	// exec(win, fail, 'BLE', 'connectedDevice', []);
    connectedDevice: function (successCallback, errorCallback, args) {
        metasense.logger.logApiEntry("connectedDevices");
        metasense.connectedDevice(successCallback, errorCallback);
        metasense.logger.logApiExit("connectedDevice");
    },
	
	// exec(win, fail, 'BLE', 'startReading', []);
    writeMessage: function (successCallback, errorCallback, args) {
        metasense.logger.logApiEntry("writeMessage");
        var address = args[0] ? args[0] : "";
        //var deviceInstanceId = args[1] ? args[1] : "";
		var message = args[1] ? args[1] : "";
        metasense.writeMessage(address, message, successCallback, errorCallback);
        metasense.logger.logApiExit("writeMessage");
    },

    // exec(win, fail, 'BLE', 'logToFile', [filename, text]);
    logToFile: function (successCallback, errorCallback, args) {
        metasense.logger.logApiEntry("logToFile");
        var filename = args[0] ? args[0] : "";
        var text = args[1] ? args[1] : "";
        metasense.logToFile(filename, text, successCallback, errorCallback);
        metasense.logger.logApiExit("logToFile");
    }
});

var metasense = {
	gatt: Windows.Devices.Bluetooth.GenericAttributeProfile,
    connectionWatcher: undefined,
    characteristic: undefined,
    onValueChanged: undefined,
    logger: {

        isDebugEnabled: true,

        baseLocation: "MetaSense bleProxy.",

        getTimeStamp: function () {
            var now = new Date();
            return (now.toLocaleDateString() + " " + now.toLocaleTimeString());
        },

        logApiEntry: function (functionName) {
            this.logDebug(functionName, "API entered");
        },

        logApiExit: function (functionName) {
            this.logDebug(functionName, "API exiting");
        },

        logDebug: function (location, msg) {
            if (metasense.logger.isDebugEnabled)
                console.log(this.getTimeStamp() + " " + this.baseLocation + location + ": " + msg);
        },

        logError: function (location, msg) {
            console.log(this.getTimeStamp() + " " + this.baseLocation + location + ": " + msg);
        }
    },
    nodeList: [],
    onDeviceConnectionUpdated: function (value) {
        var msg = "Device Connection Updated " + value;
        metasense.logger.logDebug("onDeviceConnectionUpdated", msg);
    },
    listPairedMetaSenseNodes: function (successCallback, errorCallback) {
        var serviceName = "MetaSense";
        var serviceId = "0000ffe0-0000-1000-8000-00805f9b34fb";
        // Enumerate devices. This call returns a list of all paired bluetooth devices rather than a list of devices that
        // we can actually see, so once we get each device we need to figure out of it's actually "there" by attempting
        // to connect to it.
        metasense.logger.logDebug("listPairedMetaSenseNodes", "Finding paired services...");
        var gattDevSel = Windows.Devices.Bluetooth.GenericAttributeProfile.GattDeviceService.getDeviceSelectorFromUuid(serviceId);
        Windows.Devices.Enumeration.DeviceInformation.findAllAsync(
            gattDevSel,
            ["System.Devices.Icon", "System.ItemNameDisplay", "System.Devices.Paired", "System.Devices.Connected"]).done(
            function (deviceList) {
                metasense.logger.logDebug("listPairedMetaSenseNodes", "Completed find all MetaSense Devices (success)");
                // See if we found any connected devices that support the Generic Access service
                if (deviceList.length == 0) {
                    var msg = "Could not find any " + serviceName + " devices";
                    metasense.logger.logError("scanDevices", msg);
                    errorCallback(msg, { keepCallback: true });
                    return;
                }
                // Add newly discovered devices to our list
                metasense.nodeList = [];
                var reachableCount = 0;
                for (var i = 0; i < deviceList.length; i++) {
                    if (deviceList[i].name != "MetaSense")
                        continue;
                    var device = deviceList[i];
                    Windows.Devices.Bluetooth.BluetoothLEDevice.fromIdAsync(device.id).then(
				    function (ble_device) {
				        if (ble_device) {
				            var addr = ble_device.bluetoothAddress;
				            //device.properties['System.Devices.DeviceInstanceId']
				            var nodeInfo = {
				                "name": device.name,
				                "address": addr,
				                "rssi": 0,
				                "isReachable": false,
				                "canPair": device.pairing.canPair,
				                "isPaired": device.pairing.isPaired,
				                "deviceid": device.id
				            };
				            metasense.nodeList.push(nodeInfo);
				        }
				    },
				    function (error) {
				        metasense.logger.logError("listPairedMetaSenseNodes", error);
				    });
                }
                metasense.logger.logDebug("listPairedMetaSenseNodes", "Reporting " + metasense.nodeList.length + " devices");
                // Start reporting our devices back to the caller if we haven't already done so on a previous scan
                setTimeout(function () {
                    successCallback(metasense.nodeList);
                }, 3000);
            },
            function (error) {
                metasense.logger.logDebug("listPairedMetaSenseNodes", "Completed find all MetaSense Devices (error)");
                var msg = "Windows.Devices.Enumeration.DeviceInformation.findAllAsync failed: " + error;
                metasense.logger.logError("listPairedMetaSenseNodes", msg);
                errorCallback(msg, { keepCallback: true });
            });
    },
    startReading: function (address, successCallback, errorCallback) {
        //Make sure we know when the device reconnects
        var gattDevSel = "System.Devices.DeviceInstanceId := " + address;
        //metasense.connectionWatcher = Windows.Devices.Enumeration.Pnp.PnpObject.createWatcher(
        //    Windows.Devices.Enumeration.Pnp.PnpObjectType.deviceContainer,
        //    ["System.Devices.Connected"],
        //    gattDevSel);
        //metasense.connectionWatcher.onupdated = metasense.onDeviceConnectionUpdated;
        //metasense.connectionWatcher.onadded = metasense.onDeviceConnectionUpdated;
        //metasense.connectionWatcher.onremoved = metasense.onDeviceConnectionUpdated;
        //metasense.connectionWatcher.start();
        //Enable notification
        //Windows.Devices.Bluetooth.BluetoothLEDevice.fromIdAsync(deviceInstanceId).then(
        Windows.Devices.Bluetooth.BluetoothLEDevice.fromBluetoothAddressAsync(address).then(
            function (ble_device) {
				    if (ble_device) {
				        var serviceId = "0000ffe0-0000-1000-8000-00805f9b34fb";
				        var service = ble_device.getGattService(serviceId);
				        var characteristicsId = "0000ffe1-0000-1000-8000-00805f9b34fb";
				        var charList = service.getCharacteristics(characteristicsId);
				        metasense.characteristic = charList[0];
				        if (metasense.characteristic == null) {
				            var msg = "Could not configure characteristic for notifications. No characterisitics found";
				            errorCallback(msg);
				            return;
				        }

				        // Make sure this characteristic supports sending of notifications, notify caller if not.
				        if (!(metasense.characteristic.characteristicProperties &
                            Windows.Devices.Bluetooth.GenericAttributeProfile.GattCharacteristicProperties.notify)) {
				            metasense.characteristic = undefined;
				            var msg = "This characteristic does not support notifications";
				            metasense.logger.logDebug("startReading", msg);
				            errorCallback(msg);
				            return;
				        }

				        // Create callback to handle notifications; here we will get the new value into a UTF8 buffer and send it to the caller
				        metasense.onValueChanged = function (args) {
				            var data = Uint8Array(args.characteristicValue.length);
				            Windows.Storage.Streams.DataReader.fromBuffer(args.characteristicValue).readBytes(data);
				            successCallback(data, { keepCallback: true });
				        };

				        // Register the callback
                        try {
                            metasense.characteristic.onvaluechanged = metasense.onValueChanged;
                            //metasense.characteristic.addEventListener("valuechanged", metasense.onValueChanged);
                        } catch (e) {
                            metasense.characteristic = undefined;
                            metasense.onValueChanged = undefined;
                            var msg = "Could not add event listener:" + e;
                            metasense.logger.logError("startReading", msg);
                            errorCallback(msg);
                            return;
                        }

				        // Tell the characteristic to start sending us notifications.
				        // In order to avoid unnecessary communication with the device, first determine if the device is already
				        // correctly configured to send notifications. By default ReadClientCharacteristicConfigurationDescriptorAsync
				        // will attempt to get the current value from the system cache, so physical communication with the device
				        // is not typically required.
                        metasense.characteristic.readClientCharacteristicConfigurationDescriptorAsync().then(
                            function (currentDescriptorValue) {
                                // No need to configure characteristic to send notifications if it's already configured
                                if ((currentDescriptorValue.status !== Windows.Devices.Bluetooth.GenericAttributeProfile.GattCommunicationStatus.success) ||
                                (currentDescriptorValue.clientCharacteristicConfigurationDescriptor !==
                                    Windows.Devices.Bluetooth.GenericAttributeProfile.GattClientCharacteristicConfigurationDescriptorValue.notify)) {

                                    // Set the Client Characteristic Configuration Descriptor to enable the device to send
                                    // notifications when the Characteristic value changes.
                                    metasense.logger.logDebug("startReading", "Configuring characteristic for notifications");
                                    metasense.characteristic.writeClientCharacteristicConfigurationDescriptorAsync(
                                        Windows.Devices.Bluetooth.GenericAttributeProfile.GattClientCharacteristicConfigurationDescriptorValue.notify).then(
                                        function (commStatus) {
                                            if (commStatus == Windows.Devices.Bluetooth.GenericAttributeProfile.GattCommunicationStatus.success) {
                                                metasense.logger.logDebug("startReading", "writeClientCharacteristicConfigurationDescriptorAsync completed with success");
                                            } else {
                                                var msg = "Could not configure characteristic for notifications, device unreachable. GattCommunicationStatus = " + commStatus;
                                                metasense.logger.logError("startReading", msg);
                                                errorCallback(msg);
                                            }
                                        });
                                } else {
                                    metasense.logger.logDebug("startReading", "Characteristic is already configured for notifications");
                                }
                            }
                        );
				    }
				},
				function(error) {
					metasense.logger.logError("startReading", error);
					errorCallback(error);
				});

    },
    stopReading: function (successCallback, errorCallback) {
        
		if(!metasense.characteristic) {
			metasense.onValueChanged = undefined;
            var msg = "Reading of characterisctics not started.";
			metasense.logger.logError("stopReading", msg);
            errorCallback(msg);
            return;
		}
			
		var gattDevSel = "System.Devices.DeviceInstanceId := " + deviceInstanceId;
        if (metasense.connectionWatcher) {
			metasense.connectionWatcher.stop();			
		}
		
		metasense.characteristic.onvaluechanged = undefined;
		metasense.onValueChanged = undefined;
		
		// Tell the characteristic to stop sending us notifications.
		// In order to avoid unnecessary communication with the device, first determine if the device is already
		// correctly configured to not send notifications. By default ReadClientCharacteristicConfigurationDescriptorAsync
		// will attempt to get the current value from the system cache, so physical communication with the device
		// is not typically required.
		metasense.characteristic.readClientCharacteristicConfigurationDescriptorAsync().then(
			function (currentDescriptorValue) {
				// No need to configure characteristic to send notifications if it's already configured
				if ((currentDescriptorValue.status !== Windows.Devices.Bluetooth.GenericAttributeProfile.GattCommunicationStatus.success) ||
				(currentDescriptorValue.clientCharacteristicConfigurationDescriptor !==
					Windows.Devices.Bluetooth.GenericAttributeProfile.GattClientCharacteristicConfigurationDescriptorValue.none)) {

					// Set the Client Characteristic Configuration Descriptor to enable the device to send
					// notifications when the Characteristic value changes.
					metasense.logger.logDebug("stopReading", "Disabling characteristic notifications");
					metasense.characteristic.writeClientCharacteristicConfigurationDescriptorAsync(
						Windows.Devices.Bluetooth.GenericAttributeProfile.GattClientCharacteristicConfigurationDescriptorValue.none).then(
						function (commStatus) {
							if (commStatus == Windows.Devices.Bluetooth.GenericAttributeProfile.GattCommunicationStatus.success) {
								metasense.logger.logDebug("stopReading", "writeClientCharacteristicConfigurationDescriptorAsync completed with success");
							} else {
								var msg = "Could not configure characteristic for notifications, device unreachable. GattCommunicationStatus = " + commStatus;
								metasense.logger.logError("stopReading", msg);
								errorCallback(msg);
							}
						});
				} else {
					metasense.logger.logDebug("stopReading", "Characteristic is already not configured for notifications");
				}
			});
		
		successCallback("Success");
    },
	connectedDevice: function (successCallback, errorCallback) {
		if(!metasense.characteristic) {
			var msg = "Reading of characterisctics not started.";
            metasense.logger.logError("connectedDevice", msg);
            errorCallback(msg);
		} else {
		    var addr = metasense.characteristic.service.device.bluetoothAddress;
		    //var connected = metasense.characteristic.service.device.connectionStatus.connectionStatus==1;
			successCallback(addr);
		}
    },
    writeMessage: function (address, message, successCallback, errorCallback) {
        //Make sure we know when the device reconnects
        var gattDevSel = "System.Devices.DeviceInstanceId := " + address;
		var characteristic;
        //Enable notification
        //Windows.Devices.Bluetooth.BluetoothLEDevice.fromIdAsync(deviceInstanceId).done(
		Windows.Devices.Bluetooth.BluetoothLEDevice.fromBluetoothAddressAsync(address).then(
			function (ble_device) {
				if (ble_device) {
					var serviceId = "0000ffe0-0000-1000-8000-00805f9b34fb";
					var service = ble_device.getGattService(serviceId);
					var characteristicsId = "0000ffe1-0000-1000-8000-00805f9b34fb";
					var charList = service.getCharacteristics(characteristicsId);
					characteristic = charList[0];
					if (characteristic == null) {
						var msg = "Could not write to characteristic. No characterisitics found";
						errorCallback(msg);
						return;
					}


					//var value = characteristic.protectionLevel;
					//console.log("Characteristic Protection Level ", value);
					//var value1 = service.device.deviceInformation.pairing.protectionLevel;
					//service.device.deviceInformation.pairing.protectionLevel = 1;
					//console.log("Service Pairing Protection Level ", value1);



					// Make sure this characteristic supports sending of notifications, notify caller if not.
					if (!(characteristic.characteristicProperties &
						Windows.Devices.Bluetooth.GenericAttributeProfile.GattCharacteristicProperties.writeWithoutResponse)) {
						var msg = "This characteristic does not support notifications";
						metasense.logger.logDebug("startReading", msg);
						errorCallback(msg);
						return;
					}
					
					//Create a list of strings to send
					var ret = message.match(/.{1,20}/g);
					var counter = 0;
					
					var convert = function(str) {
						var buf = new ArrayBuffer(str.length);
						var bufView = new Uint8Array(buf);
						for (var i=0, strLen=str.length; i<strLen; i++) {
							bufView[i] = str.charCodeAt(i);
						}
						return new Uint8Array(buf);
					}
					
					var recursiveWriter = function() {
						if(counter<ret.length) {
							var data = convert(ret[counter++]);
							var dataOut = Windows.Security.Cryptography.CryptographicBuffer.createFromByteArray(data);
							characteristic.writeValueAsync(dataOut, 
							Windows.Devices.Bluetooth.GenericAttributeProfile.GattWriteOption.writeWithoutResponse).then(
							function (commStatus) {
								if (commStatus == metasense.gatt.GattCommunicationStatus.success) {
									recursiveWriter();									
								} else {
									var msg = "Write failed or device unreachable, GattCommunicationStatus = " + commStatus;
									errorCallback(msg);
								}
							},
							function (error) {
								var msg = "gattChar.writeValueAsync() failed: " + error;
								metasense.logger.logError("writeCharacteristic", msg);
								errorCallback(msg);
							});
						} else {
							successCallback(counter);
						}
					}
					
					recursiveWriter();
				}
			});

    },
	initBackground: function (successCallback, errorCallback) {
        var taskRegistered = false;
        var taskName = "MetaSense Background Data and Location Tracker";
        var background = Windows.ApplicationModel.Background;
        var iter = background.BackgroundTaskRegistration.allTasks.first();

        // check if service already started
        while (iter.hasCurrent) {
            var task = iter.current.value;

            if (task.name === taskName) {
                taskRegistered = true;
                break;
            }

            iter.moveNext();
        }

        if (taskRegistered) {
            successCallback();
        } else {
            Windows.ApplicationModel.Background.BackgroundExecutionManager.requestAccessAsync().then(function () {
                var builder = new Windows.ApplicationModel.Background.BackgroundTaskBuilder();

                builder.name = taskName;
                builder.taskEntryPoint = "CordovaApp.Library.UploadTask"; // namespace of my windows runtime component library
                builder.setTrigger(new Windows.ApplicationModel.Background.TimeTrigger(15, false));
                builder.addCondition(new Windows.ApplicationModel.Background.SystemCondition(Windows.ApplicationModel.Background.SystemConditionType.internetAvailable));

                return builder.register();
            }).done(function () {
                successCallback();
            }, function (err) {
                errorCallback(err);
            });
        }

	},
	logToFile: function (filename, text, successCallback, errorCallback) {
	    // Verify that we are currently not snapped, or that we can unsnap to open the picker
	    var currentState = Windows.UI.ViewManagement.ApplicationView.value;
	    if (currentState === Windows.UI.ViewManagement.ApplicationViewState.snapped &&
            !Windows.UI.ViewManagement.ApplicationView.tryUnsnap()) {
	        // Fail silently if we can't unsnap
	        return;
	    }

	    // Create the picker object and set options
	    var savePicker = new Windows.Storage.Pickers.FileSavePicker();
	    savePicker.suggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.documentsLibrary;
	    // Dropdown of file types the user can save the file as
	    savePicker.fileTypeChoices.insert("Comma Separated Values", [".csv"]);
	    // Default file name if the user does not type one in or select a file to replace
	    savePicker.suggestedFileName = filename;

	    savePicker.pickSaveFileAsync().then(function (file) {
	        if (file) {
	            // Prevent updates to the remote version of the file until we finish making changes and call CompleteUpdatesAsync.
	            Windows.Storage.CachedFileManager.deferUpdates(file);
	            // write to file
	            Windows.Storage.FileIO.writeTextAsync(file, text).done(function () {
	                // Let Windows know that we're finished changing the file so the other app can update the remote version of the file.
	                // Completing updates may require Windows to ask for user input.
	                Windows.Storage.CachedFileManager.completeUpdatesAsync(file).done(function (updateStatus) {
	                    if (updateStatus === Windows.Storage.Provider.FileUpdateStatus.complete) {
	                        successCallback();
	                    } else {
	                        errorCallback("File " + file.name + " couldn't be saved.");
	                    }
	                });
	            });
	        } else {
	            errorCallback("Log to File Operation cancelled.");
	        }
	    });
	}
}

