// API definition for EvoThings BLE plugin.
//
// Use jsdoc to generate documentation.

// The following line causes a jsdoc error.
// Use the jsdoc option -l to ignore the error.
var exec = cordova.require('cordova/exec');

/** @module edu.ucsd.MetaSense.ble */



//***************************************
// Max MetaSense Node BLE plugin Module
//***************************************

/** Get a list of paired MetaSense Devices.
* <p>Found devices and errors will be reported to the supplied callbacks.</p>
* <p>Will keep scanning indefinitely until you call stopScan().</p>
* To conserve energy, call stopScan() as soon as you've found the device you're looking for.
* <p>Calling this function while scanning is in progress has no effect?</p>
*
* @param {scanCallback} win
* @param {failCallback} fail
*
* @example
metasense.ble.listPairedMetaSenseNodes(
	function(deviceList)
	{
   		console.log('ListPairedMetaSenseNodes - Metasense nodes found.\n');
        for(var i=0; i<deviceList.length; i++)
    		console.log('   ' + deviceList[i]);
	},
	function(errorCode)
	{
   		console.log('ListPairedMetaSenseNodes - error: ' + errorCode);
	}
);
*/

exports.listPairedMetaSenseNodes = function (win, fail) {
    exec(win, fail, 'BLE', 'listPairedMetaSenseNodes', []);
};


/** Info about a MetaSense Node.
* @typedef {Object} NodeInfo
* @property {string} address - Uniquely identifies the device.
* The form of the address depends on the host platform.
* @property {string} serviceAddress - Uniquely identifies the messaging service on the device.
* @property {string} name - The device's name, or nil.
* @property {number} rssi - A negative integer, the signal strength in decibels.
* @property {string} deviceid - Node Serial.
*/
    
/** This function is called when an operation fails.
* @callback failCallback
* @param {string} errorString - A human-readable string that describes the error that occurred.
*/

exports.startReading= function (address, win, fail) {
    exec(win, fail, 'BLE', 'startReading', [address]);
};

exports.stopReading= function (win, fail) {
    exec(win, fail, 'BLE', 'stopReading', []);
};

exports.connectedDevice = function (win, fail) {
    exec(win, fail, 'BLE', 'connectedDevice', []);
};

exports.writeMessage= function (address, msg, win, fail) {
    exec(win, fail, 'BLE', 'writeMessage', [address, msg]);
};

exports.logToFile = function (filename, text, win, fail) {
    exec(win, fail, 'BLE', 'logToFile', [filename, text]);
};